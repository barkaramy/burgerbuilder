import React from 'react';
import Logo from '../../Logo/Logo';
import NavigationItems from '../../Navigation/NavigationItems/NavigationItems';
import classes from './SideDrawer.css';
const sideDrawer = (props) => {
  return (
    <div className={classes.sideDrawer}>
      <div className={classes.Logo}>
        <Logo />
      </div>

      <div>
        <NavigationItems />
      </div>
    </div>
  );
};

export default sideDrawer;
